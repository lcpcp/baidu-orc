<?php

namespace End01here\BaiDuOrc;

use Illuminate\Support\ServiceProvider;

class BaiDuOrcServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //注册清除服务
//        $this->commands([
//            ClearFileCodeCommand::class
//        ]);

        // 单例绑定服务
//        $this->app->singleton('command.eternaltree.install', function ($app) {
//            return new ClearFileCodeCommand( );
//        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([
            __DIR__.'/Config/baidu_orc.php' => config_path('baidu_orc.php'), // 发布配置文件到 laravel 的config 下
        ]);
    }
}
