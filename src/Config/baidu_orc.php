<?php

return [
    //code码验证存储方式
    'app_id' => env('BAI_APP_ID',''),
    //短信有效时长（分钟）
    'api_key' => env('BAI_API_KEY',''),
    'secret_key' => env('BAI_SECRET_KEY',''),
];
