<h1 align="center"> 百度ORC识别API封装 </h1>


### 使用
* 安装
```
composer require end01here/baidu-orc
```
* 相关配置
```
BAI_APP_ID= 
BAI_API_KEY= 
BAI_SECRET_KEY= 
```
* 部署配置文件
```
php artisan vendor:publish --provider="End01here\BaiDuOrc\BaiDuOrcServiceProvider"
```
* Api使用
```php
           $phone = BaiDuOrcService::getOrcServer()->basicGeneralUrl('https://wealfavor-1257406827.cos.ap-beijing.myqcloud.com/app-serve/2022-10-12/gee1gshrrq.jpg');
                dd($phone);
```
